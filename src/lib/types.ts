/*
    MESSAGE XP
*/
export type WeakMapMessage = {
    tupperAuthor?: string;
};
export type DbXPSettings = {
    noXPChannels: string[];
    noXPCategories: string[];
    noXPMessagePrefixes: string[];
    
    minimumLength: number;
    maximumCharactersPerSecond: number;
}
export type MessageXP = {
    value: number;
    explanations: MessageXPExplanation[];
}
export type MessageXPExplanation = {
    value: number;
    explanation: string;
}