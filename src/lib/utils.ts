import type { ILogger } from '@sapphire/framework';
import { send } from '@sapphire/plugin-editable-commands';
import { Message, MessageEmbed, User } from 'discord.js';
import type { Level } from 'level';
import { RandomLoadingMessage, TupperSnowflake } from './constants';
import type { DbXPSettings, MessageXP, WeakMapMessage } from './types';

import { DateTime } from 'luxon';
import * as SpellChecker from 'spellchecker';
/// import writeGood from 'write-good';

/**
 * Picks a random item from an array
 * @param array The array to pick a random item from
 * @example
 * const randomEntry = pickRandom([1, 2, 3, 4]) // 1
 */
export function pickRandom<T>(array: readonly T[]): T {
	const { length } = array;
	return array[Math.floor(Math.random() * length)];
}

/**
 * Sends a loading message to the current channel
 * @param message The message data for which to send the loading message
 */
export function sendLoadingMessage(message: Message): Promise<typeof message> {
	return send(message, { embeds: [new MessageEmbed().setDescription(pickRandom(RandomLoadingMessage)).setColor('#FF0000')] });
}

/**
 * Load XP settings for a guild, or return default values if a db record does not exist.
 * @param db The Level DB containing XP Settings
 * @param guildId The Guild Snowflake.
 * @param logger The ILogger for logging output
 * @returns Settings for the provided guild.
 */
export async function loadGuildXPSettings(db: Level, guildId: string, logger: ILogger): Promise<DbXPSettings> {
	// Default values for guild XP Settings
	let guildXPSettings: DbXPSettings = {
		noXPChannels: [],
		noXPCategories: [],
		noXPMessagePrefixes: ['/', '('],
		minimumLength: 140,
		maximumCharactersPerSecond: 7
	};

	// Load XP Settings for this guild
	try {
		guildXPSettings = await db.get(guildId, { valueEncoding: 'json' });
	} catch (e) {
		// Assume guild does not have a settings record if an error occurs; just ignore.
		// ! WARNING! ANY error will reset guild settings!
		// TODO: Fix the above once you figure out how the F TypeScript does this.
		logger.warn(
			`messageCreateXP: Error occurred trying to access XP settings for guild ${guildId}. Assuming guild does not exist and using default values.`
		);
	}

	return guildXPSettings;
}

/**
 * Calculate how much XP a message should earn.
 * @param message The discord message to calculate
 * @param author The author of the message
 * @param guildXPSettings The guild's XP settings
 * @param containerMessages client.container.messages
 * @returns A MessageXP containing information about the XP for this message.
 */
export async function calculateMessageXP(
	message: Message,
	author: User,
	guildXPSettings: DbXPSettings,
	containerMessages: WeakMap<Message, WeakMapMessage>
): Promise<MessageXP> {
	// * First, we need to calculate if the member exceeded the characters per second limit. Skip if content length is 0;
	if (message.cleanContent.length > 0) {
		// Determine the minimum amount of time a message should have taken in seconds to write to be allowed XP.
		let messageTime = message.cleanContent.length / guildXPSettings.maximumCharactersPerSecond;

		// Find the previous most recent message sent in the same channel by the same author.
		let previousMessageUser = message.channel.messages.cache
			.filter((a) => a.author.id === author.id && a.createdTimestamp < message.createdTimestamp) // Only include messages by the same author sent before the message we are dealing with.
			.sort((a, b) => b.createdTimestamp - a.createdTimestamp) // Put newest messages first
			.find((a) => a.id !== message.id); // Return the first message that is not the same as the message we are calculating.

		// Also search Tupper messages
		let previousMessageTupper = message.channel.messages.cache
			.filter((a) => a.author.id === TupperSnowflake && a.createdTimestamp < message.createdTimestamp)
			.sort((a, b) => b.createdTimestamp - a.createdTimestamp) // Put newest messages first
			.find((a) => {
				let original = containerMessages.get(a);
				if (!original || !original.tupperAuthor || original.tupperAuthor !== author.id) return false;

				return true;
			});

		// Determine if the User or the Tupper message came most recent
		let previousMessage: Message | undefined = undefined;
		if (previousMessageUser && previousMessageTupper) {
			previousMessage = previousMessageUser.id > previousMessageTupper.id ? previousMessageUser : previousMessageTupper;
		} else if (previousMessageUser) {
			previousMessage = previousMessageUser;
		} else if (previousMessageTupper) {
			previousMessage = previousMessageTupper;
		}

		// If a previous message exists, determine if the characters per second maximum is exceeded.
		if (previousMessage) {
			let timediff = DateTime.fromJSDate(message.createdAt).diff(DateTime.fromJSDate(previousMessage.createdAt));
			if (timediff.as('seconds') <= messageTime) {
				return {
					value: 0,
					explanations: [
						{
							value: 0,
							explanation:
								'Message earned no XP; author exceeded maximum allowed characters per second speed compared to their previous message.'
						}
					]
				};
			}
		}
	}

	// * Per request, capitals / shouting is not being checked.
	// * Per request, XP is neither awarded nor penalized for non-text components of messages.

	// We are going to separate actual content length from calculated length used for determining XP award.
	let calculatedLength = message.cleanContent.length;

	// Check misspellings
	let misspellings = await SpellChecker.checkSpellingAsync(message.cleanContent);

	// For every misspelled word, remove its length from the official calculated length used for awarding XP.
	misspellings.forEach((misspelling) => {
		calculatedLength -= misspelling.end - misspelling.start;
	})

	// * Currently, grammar checking is not possible; LanguageTool public API is severely restrictive and the self-hosted version is out of date and runs as a JAVA container. Furthermore, other libraries are not TS compatible.



	return {
		value: 0,
		explanations: []
	};
}
