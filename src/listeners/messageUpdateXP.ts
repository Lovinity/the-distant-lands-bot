import { ApplyOptions } from '@sapphire/decorators';
import { Listener, ListenerOptions } from '@sapphire/framework';
import type { Message, TextChannel } from 'discord.js';
import { TupperSnowflake } from '../lib/constants';
import { calculateMessageXP, loadGuildXPSettings } from '../lib/utils';

@ApplyOptions<ListenerOptions>({
	event: 'messageUpdate'
})
export class MessageUpdateXP extends Listener {
	public async run(oldMessage: Message, message: Message) {
		if (!message.guild) return; // Skip messages not from a guild

		// If this was a bot message not made by Tupper, check its webhook.
		let isTupperWebhook = false;
		if (message.author.bot && message.author.id !== TupperSnowflake) {
      if (!message.webhookId) return;
			let webhook = await message.fetchWebhook();

			// No webhook, or webhook not owned by Tupper? Skip the message.
			if (!webhook || !webhook.owner || webhook.owner.id !== TupperSnowflake) return;

			// At this point, assume webhook is owned by Tupper.
			isTupperWebhook = true;
		}

		// If message came from Tupper, see if we can find the original author. If so, resolve that author. Else, skip message.
		let author = message.author;
		if (author.id === TupperSnowflake || isTupperWebhook) {
			this.container.logger.info(`messageUpdateXP: Message ${message.id}: Tupperbox. Checking if message was created as a Tupper...`);
			let eMessage = this.container.messages.get(message);
			if (eMessage && eMessage.tupperAuthor) {
				author = await this.container.client.users.fetch(eMessage.tupperAuthor);
			} else {
				this.container.logger.info(`messageUpdateXP: Message ${message.id}: Tupperbox. No author pairings found; message skipped.`);
				return;
			}
		} else {
			// Add to pending messages in case Tupper deleted it to convert to a Tupper.
			this.container.pendingTupperMap.push(message);
		}

		// Load guild XP Settings
		let guildXPSettings = await loadGuildXPSettings(this.container.db.XP.settings, message.guild.id, this.container.logger);

		// Skip if in a channel that should be ignored
		if (guildXPSettings.noXPChannels.indexOf(message.channel.id) !== -1) return;

		// Skip if the channel is in a category that should be ignored.
		// * I prefer to use parent.id instead of parentId due to Discord.js's habit of deprecating shortcut properties.
		// ! Also, we have to set the parent as a variable or undefined due to the possibility of it being null.
		let parent = (message.channel as TextChannel).parent || undefined;
		if (parent && guildXPSettings.noXPCategories.indexOf(parent.id) !== -1) return;

		// Calculate the XP for this message and the previous old message
		let oldXP = await calculateMessageXP(oldMessage, author, guildXPSettings, this.container.messages);
		let newXP = await calculateMessageXP(message, author, guildXPSettings, this.container.messages);
		this.container.logger.debug(`messageUpdateXP: Message XP to be changed: `, oldXP, newXP);

		// Adjust XP
		let guildXP = this.container.db.XP.data.sublevel<string, number>(message.guild.id, { valueEncoding: 'json' });
		let memberXP: number = 0;
		try {
			memberXP = await guildXP.get(author.id);
		} catch (e) {
			// Assume member has no XP if an error occurs; just ignore.
			// WARNING! ANY error will reset a member's XP to 0!
			// TODO: Fix the above once you figure out how the F TypeScript does this.
			this.container.logger.warn(
				`messageUpdateXP: Error occurred trying to access member ${author.id}. Assuming member does not exist and setting XP to 0.`
			);
		}
		await guildXP.put(author.id, memberXP + (newXP.value - oldXP.value));

		await message.reply(`You now have ${await guildXP.get(author.id)} XP.`);
	}
}
