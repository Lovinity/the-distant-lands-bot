import { ApplyOptions } from '@sapphire/decorators';
import { Listener, ListenerOptions } from '@sapphire/framework';
import type { Message, TextChannel } from 'discord.js';
import { TupperSnowflake } from '../lib/constants';
import { calculateMessageXP, loadGuildXPSettings } from '../lib/utils';

@ApplyOptions<ListenerOptions>({
	event: 'messageDelete'
})

/**
 * Manage the removal of XP for deleting a message, or manage Tupper mappings for XP.
 */
export class MessageDeleteXP extends Listener {
	public async run(message: Message) {
		if (!message.guild) return; // Skip messages not from a guild

		// If this was a bot message not made by Tupper, check its webhook.
		// * For message delete, we have to fetch the webhook on the client side since the message is deleted.
		let isTupperWebhook = false;
		if (message.author.bot && message.author.id !== TupperSnowflake) {
			if (!message.webhookId) return;
			let webhook = await this.container.client.fetchWebhook(message.webhookId);

			// No webhook, or webhook not owned by Tupper? Skip the message.
			if (!webhook || !webhook.owner || webhook.owner.id !== TupperSnowflake) return;

			// At this point, assume webhook is owned by Tupper.
			isTupperWebhook = true;
		}

		// If message came from Tupper, see if we can find the original author. If so, resolve that author. Else, skip message.
		let author = message.author;
		if (author.id === TupperSnowflake || isTupperWebhook) {
			this.container.logger.info(`messageDeleteXP: Message ${message.id}: Tupperbox. Checking if message was created as a Tupper...`);
			let eMessage = this.container.messages.get(message);
			if (eMessage && eMessage.tupperAuthor) {
				author = await this.container.client.users.fetch(eMessage.tupperAuthor);
				this.container.logger.info(`messageDeleteXP: Message ${message.id}: Tupperbox. Belongs to ${author.tag}`);
			} else {
				this.container.logger.info(`messageDeleteXP: Message ${message.id}: Tupperbox. No author pairings found; message skipped.`);
				return;
			}
		} else {
			// Check if maybe Tupper already created a message for this
			let originalMessage = this.container.pendingTupperMap.find((a) => message.cleanContent.includes(a.cleanContent));
			if (originalMessage) {
				let id = originalMessage.id;

				// Map this tupper message with the original author in cache
				this.container.messages.set(originalMessage, Object.assign(this.container.messages.get(originalMessage) || {}, { tupperAuthor: author.id }));

				// Remove pending tupper message
				this.container.pendingTupperMap.filter((a) => a.id !== id);
				this.container.logger.info(
					`messageDeleteXP: Message ${message.id}: Message mapped to Tupperbox message ${id}.`
				);
				return; // Do not continue / remove XP for mapped messages.
			} else {
				this.container.pendingTupperMap.push(message);
				this.container.logger.info(`messageDeleteXP: Message ${message.id}: Not a Tupper message that I know; added to pending in case we are waiting for Tupper to post.`);
			}
		}

		// Load guild XP Settings
		let guildXPSettings = await loadGuildXPSettings(this.container.db.XP.settings, message.guild.id, this.container.logger);

		// Skip if in a channel that should be ignored
		if (guildXPSettings.noXPChannels.indexOf(message.channel.id) !== -1) return;

		// Skip if the channel is in a category that should be ignored.
		// * I prefer to use parent.id instead of parentId due to Discord.js's habit of deprecating shortcut properties.
		// ! Also, we have to set the parent as a variable or undefined due to the possibility of it being null.
		let parent = (message.channel as TextChannel).parent || undefined;
		if (parent && guildXPSettings.noXPCategories.indexOf(parent.id) !== -1) return;

		// Calculate the XP for this message
		let XP = await calculateMessageXP(message, author, guildXPSettings, this.container.messages);
		this.container.logger.debug(`messageDeleteXP: Message XP to be removed: `, XP);

		// Remove XP
		let guildXP = this.container.db.XP.data.sublevel<string, number>(message.guild.id, { valueEncoding: 'json' });
		let memberXP: number = 0;
		try {
			memberXP = await guildXP.get(author.id);
		} catch (e) {
			// Assume member has no XP if an error occurs; just ignore.
			// WARNING! ANY error will reset a member's XP to 0!
			// TODO: Fix the above once you figure out how the F TypeScript does this.
			this.container.logger.warn(
				`messageDeleteXP: Error occurred trying to access member ${author.id}. Assuming member does not exist and setting XP to 0.`
			);
		}
		await guildXP.put(author.id, memberXP - XP.value);

		await message.channel.send(`You now have ${await guildXP.get(author.id)} XP.`);
	}
}
