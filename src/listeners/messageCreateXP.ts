import { ApplyOptions } from '@sapphire/decorators';
import { Listener, ListenerOptions } from '@sapphire/framework';
import type { Message, TextChannel } from 'discord.js';
import { TupperSnowflake } from '../lib/constants';

import { calculateMessageXP, loadGuildXPSettings } from '../lib/utils';

@ApplyOptions<ListenerOptions>({
	event: 'messageCreate'
})

/**
 * Award XP for text messages sent
 */
export class MessageCreateXP extends Listener {
	public async run(message: Message) {
		if (!message.guild) return; // Skip messages not from a guild

		// If this was a bot message not made by Tupper, check its webhook.
		let isTupperWebhook = false;
		if (message.author.bot && message.author.id !== TupperSnowflake) {
			if (!message.webhookId) return;
			let webhook = await message.fetchWebhook();

			// No webhook, or webhook not owned by Tupper? Skip the message.
			if (!webhook || !webhook.owner || webhook.owner.id !== TupperSnowflake) return;

			// At this point, assume webhook is owned by Tupper.
			isTupperWebhook = true;
		}

		// If message came from Tupper, see if we can match it to the original author. If we cannot, skip this message.
		let author = message.author;
		if (message.author.id === TupperSnowflake || isTupperWebhook) {
			this.container.logger.info(`messageCreateXP: Message ${message.id}: Tupperbox. Searching for original author...`);

			let originalMessage = this.container.pendingTupperMap.find((a) => a.cleanContent.includes(message.cleanContent));
			if (originalMessage) {
				let id = originalMessage.id;

				// Set author to original author instead for XP calculations.
				author = originalMessage.author;

				// Map this tupper message with the original author in cache
				this.container.messages.set(message, Object.assign(this.container.messages.get(message) || {}, { tupperAuthor: author.id }));

				// Remove pending tupper message
				this.container.pendingTupperMap.filter((a) => a.id !== id);
				this.container.logger.info(
					`messageCreateXP: Message ${message.id}: Tupperbox message mapped to author ${originalMessage.author.tag}.`
				);
			} else {
				this.container.pendingTupperMap.push(message);
				this.container.logger.info(`messageCreateXP: Message ${message.id}: Tupperbox message original author not found! We might be waiting for the delete event; added to pending.`);
				return;
			}
		}

		// Load guild XP Settings
		let guildXPSettings = await loadGuildXPSettings(this.container.db.XP.settings, message.guild.id, this.container.logger);

		// Skip if in a channel that should be ignored
		if (guildXPSettings.noXPChannels.indexOf(message.channel.id) !== -1) return;

		// Skip if the channel is in a category that should be ignored.
		// * I prefer to use parent.id instead of parentId due to Discord.js's habit of deprecating shortcut properties.
		// ! Also, we have to set the parent as a variable or undefined due to the possibility of it being null.
		let parent = (message.channel as TextChannel).parent || undefined;
		if (parent && guildXPSettings.noXPCategories.indexOf(parent.id) !== -1) return;

		// Calculate the XP for this message
		let XP = await calculateMessageXP(message, author, guildXPSettings, this.container.messages);
		this.container.logger.debug(`messageCreateXP: Message XP to be awarded: `, XP);

		// Award XP
		let guildXP = this.container.db.XP.data.sublevel<string, number>(message.guild.id, { valueEncoding: 'json' });
		let memberXP: number = 0;
		try {
			memberXP = await guildXP.get(author.id);
		} catch (e) {
			// Assume member has no XP if an error occurs; just ignore.
			// WARNING! ANY error will reset a member's XP to 0!
			// TODO: Fix the above once you figure out how the F TypeScript does this.
			this.container.logger.warn(
				`messageCreateXP: Error occurred trying to access member ${author.id}. Assuming member does not exist and setting XP to 0.`
			);
		}
		await guildXP.put(author.id, memberXP + XP.value);

		await message.reply(`You now have ${await guildXP.get(author.id)} XP.`);
	}
}
