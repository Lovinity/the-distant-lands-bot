import './lib/setup';
import { container, LogLevel, SapphireClient } from '@sapphire/framework';

import { Level } from 'level';
import type { Message } from 'discord.js';
import type { WeakMapMessage } from './lib/types';
import { DateTime } from 'luxon';

// Declare custom types for Sapphire Pieces
declare module '@sapphire/pieces' {
	interface Container {
		db: {
			XP: {
				settings: Level<string, any>;
				data: Level<string, any>;
			};
		};
		messages: WeakMap<Message, WeakMapMessage>;
		pendingTupperMap: Message[];
	}
}

// Construct a custom bot client
class BotClient extends SapphireClient {
	public constructor() {
		// Call Sapphire Client with our client options
		super({
			defaultPrefix: '!',
			regexPrefix: /^(hey +)?bot[,! ]/i,
			caseInsensitiveCommands: true,
			logger: {
				level: LogLevel.Debug
			},
			shards: 'auto',
			intents: ['GUILDS', 'GUILD_MESSAGES', 'GUILD_MESSAGE_REACTIONS', 'DIRECT_MESSAGES', 'DIRECT_MESSAGE_REACTIONS']
		});

		// Construct a JSON database
		// NOTE: We cannot use sublevels here; level does not export AbstractSublevel, so we have to use them in the relevant places we access the database
		container.db = {
			XP: {
				settings: new Level('.db/XP_settings', { valueEncoding: 'json' }),
				data: new Level('.db/XP_data', { valueEncoding: 'json' })
			}
		};

		// Construct the WeakMap for extending messages (WeakMap because once Message is garbage collected, we don't want its values anymore)
		container.messages = new WeakMap();

		// Construct an array for mapping Tupper messages to the original author (deleted messages are thrown in here until this.pendingTupperMapCleaner sweeps it or Tupper posts a message very similar to one in here).
		container.pendingTupperMap = [];

		// Set cleaner for pending messages (remove messages older than 1 minute)
		setInterval(() => {
			container.pendingTupperMap = container.pendingTupperMap.filter((a) => DateTime.now().diff(DateTime.fromJSDate(a.createdAt)).seconds < 60);
		}, 60000);
	}
}
const client = new BotClient();

const main = async () => {
	try {
		client.logger.info('Logging in');
		await client.login();
		client.logger.info('logged in');
	} catch (error) {
		client.logger.fatal(error);
		client.destroy();
		process.exit(1);
	}
};

main();
